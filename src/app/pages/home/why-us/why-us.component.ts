import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'app-why-us',
  templateUrl: './why-us.component.html',
  styleUrls: ['./why-us.component.scss']
})
export class WhyUsComponent implements OnInit {
  whyUs = [];


  private unSubscribe$ = new Subject();


  constructor(private network: NetworkService) { }
  
  ngOnInit(): void {
    this.getAllWhyUs();
  }
  getAllWhyUs(){
    this.network.getWhyUs().pipe(takeUntil(this.unSubscribe$)).subscribe((response: any) => {
      this.whyUs = response.data;
      console.log('Response of WhyUs: ', response);
      
    })
  }

  ngOnDestroy(){
    this.unSubscribe$.complete();
  }

}
