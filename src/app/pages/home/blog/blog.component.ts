import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { NetworkService } from "src/app/services/network.service";

@Component({
  selector: "app-blog",
  templateUrl: "./blog.component.html",
  styleUrls: ["./blog.component.scss"],
})
export class BlogComponent implements OnInit, OnDestroy {
  breakpoint: number;
  blogs = [];

  private unSubscribe$ = new Subject();

  constructor(private network: NetworkService) {}

  ngOnInit(): void {
    this.breakpoint = window.innerWidth <= 400 ? 1 : 2;
    this.getBlogs();
  }

  getBlogs() {
    this.network
      .getBlogs()
      .pipe(takeUntil(this.unSubscribe$))
      .subscribe((response: any) => {
        this.blogs = response.post.filter((x) => x.slug != "about-us");
        if (this.blogs.length >= 3) {
          this.blogs.length = 3;
        }
        console.log("Response: Blogs", this.blogs);
      });
  }

  getDay(dateReceived: any) {
    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    const d = new Date(dateReceived);
    const year = d.getFullYear(); // 2019
    const date = d.getDate(); // 23
    const monthName = months[d.getMonth()];
    const dayName = days[d.getDay()]; // Thu
    const formatted = `${dayName}, ${date} ${monthName} ${year}`;

    return formatted;
  }

  onResize(event) {
    this.breakpoint = event.target.innerWidth <= 400 ? 1 : 2;
  }

  ngOnDestroy() {
    this.unSubscribe$.complete();
  }
}
