import { AfterViewInit, Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';



const ELEMENT_DATA = [
  {position: 1, name: 'Emaar', weight: '89%', symbol: 'Emaar Development P.J.S.C  '},
  {position: 2, name: 'Arabian Ranches - Samara Community	', weight: '76%', symbol: 'Emaar Development P.J.S.C  '},
  {position: 3, name: 'Arabian Ranches-Casa Community', weight: '50%', symbol: 'Emaar Development P.J.S.  '},
  {position: 4, name: 'Green Community  West-  Extension- Phase Iii', weight: '30%', symbol: 'Properties Investment (L.L.C.)  '},
  {position: 5, name: 'Reem-Mira Community Ph5', weight: '20%', symbol: 'Al Aryam Phase One L.L.C'},

]

@Component({
  selector: 'app-offplan-home',
  templateUrl: './offplan-home.component.html',
  styleUrls: ['./offplan-home.component.scss']
})
export class OffplanHomeComponent implements OnInit {
  @ViewChild("animatedDigit") animatedDigit: ElementRef;
  @Input('count') count: number;
  steps: number;


  _projects: any[];
  get projects(): any[] {
      return this._projects;
  }
  @Input('projects') set projects(value: any[]) {
      console.log({value});
      
      this._projects = value;
      this.updatePeriodTypes();
  }

  duration: number = 1000;
  displayedColumns: string[] = ['id', 'title', 'address', 'delivery_date'];
  dataSource;
  showTable = false;
  options = [
    {name: 'Emaar'},
    {name: 'Black Orchid'},
    {name: 'Western Residence - North - 25 Villas	'}
  ];
  filteredOptions: Observable<any[]>;

  constructor(){
    
    if (this.count) {
      this.animateCount();
    }
    
  }

  ngOnInit() {
    
  }

  updatePeriodTypes(){
    
    this.dataSource = new MatTableDataSource(this.projects)
  }


  displayFn(user: any): string {
    return user && user.name ? user.name : '';
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if(filterValue.length > 1) {
      this.showTable = true
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    else{
      this.showTable = false;
      this.dataSource.filter = null;

    }
  }
  

  animateCount() {
    if (!this.duration) {
      this.duration = 1000;
    }

    if (typeof this.count === "number") {
      this.counterFunc(this.count, this.duration, this.animatedDigit);
    }
  }

  counterFunc(endValue, durationMs, element) {
    if (!this.steps) {
      this.steps = 12;
    }

    const stepCount = Math.abs(durationMs / this.steps);
    const valueIncrement = (endValue - 0) / stepCount;
    const sinValueIncrement = Math.PI / stepCount;

    let currentValue = 0;
    let currentSinValue = 0;

    function step() {
      currentSinValue += sinValueIncrement;
      currentValue += valueIncrement * Math.sin(currentSinValue) ** 2 * 2;
      if(element){
        element.nativeElement.textContent = Math.abs(Math.floor(currentValue));
      }  

      if (currentSinValue < Math.PI) {
        window.requestAnimationFrame(step);
      }
    }

    step();
  }

  ngAfterViewInit() {
    if (this.count) {
      this.animateCount();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["count"]) {
      this.animateCount();
    }
  }
}
