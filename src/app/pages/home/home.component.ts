import { Component, OnDestroy, OnInit } from '@angular/core';
import { Settings, AppSettings } from '../../app.settings';
import { AppService } from '../../app.service';
import { Property, Pagination, Location } from '../../app.models';
import { filter, map, takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { MediaChange, MediaObserver } from '@angular/flex-layout'; 
import { debounceTime, distinctUntilChanged } from 'rxjs/operators'; 
import { NetworkService } from 'src/app/services/network.service';
import { ProjectService } from 'src/app/services/data-services/project.service';
import { AreaService } from 'src/app/services/data-services/areas.service';
import { BasePage } from '../base-page/base-page';
import { Injector } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BasePage implements OnInit, OnDestroy {

  watcher: Subscription;
  activeMediaQuery = ''; 
  private unsubscribe$ = new Subject()

  public slides = [];
  public projects = [];
  public offPlanProjects: any = [];
  offPlanCounts = 0;
  public areas: any = [];


  public properties: any[];
  public viewType: string = 'grid';
  public viewCol: number = 25;
  public count: number = 8;
  public sort: string;
  public searchFields: any;
  public removedSearchField: string;
  public pagination:Pagination = new Pagination(1, 8, null, 2, 0, 0); 
  public message:string;
  public featuredProperties: Property[];
  public locations: Location[]; 

  public settings: Settings;
  constructor(public injector: Injector,public appSettings:AppSettings, public appService:AppService, public network:NetworkService, public projectService:ProjectService,public area:AreaService, public mediaObserver: MediaObserver) {
    super(injector);
    this.settings = this.appSettings.settings;
    this.watcher = mediaObserver.asObservable()
    .pipe(filter((changes: MediaChange[]) => changes.length > 0), map((changes: MediaChange[]) => changes[0]))
    .subscribe((change: MediaChange) => {
      // console.log(change)
      if(change.mqAlias == 'xs') {
        this.viewCol = 100;
      }
      else if(change.mqAlias == 'sm'){
        this.viewCol = 50;
      }
      else if(change.mqAlias == 'md'){
        this.viewCol = 33.3;
      }
      else{
        this.viewCol = 25;
      }
    });

  }

  ngOnInit() {  
    this.getSlides();
    this.getLocations();
    this.getProperties();  
    this.getFeaturedProperties();
    this.getOffPlanProjects();
    this.getAreas();
    
  }

  ngDoCheck(){
    // if(this.settings.loadMore.load){     
    //   this.settings.loadMore.load = false;     
    //   this.getProperties();  
    // }
  }

 
  public getSlides(){
    this.network.getSliders().pipe(takeUntil(this.unsubscribe$)).subscribe((res: any)=> {
      console.log('Sliders: Hello',res);
      this.slides = res.data;
      
    })
  }

  async getAreas(){
    this.areas = await this.area.getAreas();
  }

  async getOffPlanProjects(){
    this.offPlanProjects = await this.projectService.getOffPlanProjects();
    this.offPlanCounts = this.offPlanProjects.length;
  }

  public getLocations(){
    this.appService.getLocations().subscribe(res =>{
      this.locations = res;
    })
  }

  public getProperties(){  
   

    this.network.getProperties().pipe(takeUntil(this.unsubscribe$)).subscribe((response: any) => {
      this.properties = response.data;
    console.log('get properties by : ', response.data);  

      if(this.settings.header == 'map'){
        this.locations.length = 0;
        this.properties.forEach(p => {
          let loc = new Location(p.id, p.location.lat, p.location.lng);
          this.locations.push(loc);
        });
        this.locations = [...this.locations];
      } 
     
      
    })
  }

  public resetLoadMore(){
    this.settings.loadMore.complete = false;
    this.settings.loadMore.start = false;
    this.settings.loadMore.page = 1;
    this.pagination = new Pagination(1, this.count, null, null, this.pagination.total, this.pagination.totalPages);
  }

  public filterData(data){
    return this.appService.filterData(data, this.searchFields, this.sort, this.pagination.page, this.pagination.perPage);
  }

  public searchClicked(event){
    console.log({event});
    
    let types = [];
    if(event.propertyType != null){
      for (const iterator of event.propertyType) {
        types.push(iterator.name)
      }
    }
    this.nav.navigateTo('/properties', {queryParams: {filters: types, priceFrom: event.price.from, priceTo: event.price.to, from: 'home'} });
  }

  public searchChanged(event){    
    event.valueChanges.subscribe(() => {
      this.resetLoadMore();
      this.searchFields = event.value;
      setTimeout(() => {      
        this.removedSearchField = null;
      });
      if(!this.settings.searchOnBtnClick){     
        this.properties.length = 0;  
      }            
    }); 
    event.valueChanges.pipe(debounceTime(500), distinctUntilChanged()).subscribe(() => { 
      if(!this.settings.searchOnBtnClick){     
        this.getProperties(); 
      }
    });       
  } 
  public removeSearchField(field){ 
    this.message = null;   
    this.removedSearchField = field; 
  } 
 


  public changeCount(count){
    this.count = count;
    this.resetLoadMore();   
    this.properties.length = 0;
    this.getProperties();

  }
  public changeSorting(sort){    
    this.sort = sort;
    this.resetLoadMore(); 
    this.properties.length = 0;
    this.getProperties();
  }
  public changeViewType(obj){ 
    this.viewType = obj.viewType;
    this.viewCol = obj.viewCol; 
  }


  public getFeaturedProperties(){
    this.appService.getFeaturedProperties().subscribe(properties=>{
      this.featuredProperties = properties;
    })
  } 

  ngOnDestroy(){
    this.resetLoadMore();
    this.watcher.unsubscribe();
    this.unsubscribe$.complete();
  }


}
