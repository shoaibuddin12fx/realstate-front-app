import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuildersComponent } from './builders.component';
import { AgmCoreModule } from '@agm/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

export const routes = [
  { path: '', component: BuildersComponent, pathMatch: 'full' }
]


@NgModule({
  declarations: [BuildersComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AgmCoreModule,
    SharedModule
  ]
})
export class BuildersModule { }
