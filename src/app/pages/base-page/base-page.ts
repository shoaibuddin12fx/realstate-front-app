import { NavService } from 'src/app/services/nav.service';
import { Injector } from '@angular/core';
import { NetworkService } from 'src/app/services/network.service';
import { UtilityService } from 'src/app/services/utility.service';
import { Location } from '@angular/common';
import { EventsService } from 'src/app/services/events.service';
import { FormBuilder } from '@angular/forms';
import { PopoversService } from 'src/app/services/basic/popovers.service';
import { UserService } from 'src/app/services/user.service';
import { ModalService } from 'src/app/services/basic/modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SqliteService } from 'src/app/services/sqlite.service';
import { PropertyService } from 'src/app/services/data-services/property.service';

export abstract class BasePage {

    public network: NetworkService;
    public utility: UtilityService;
    public nav: NavService;
    public location: Location;
    public events: EventsService;
    // public formBuilder: FormBuilder;
    public popover: PopoversService;
    public users: UserService;
    public modals: ModalService;
    public router: Router;
    public activatedRoute: ActivatedRoute;
    public sqlite: SqliteService;
    public propertyService: PropertyService


    constructor(injector: Injector) {
        //Services
        this.users = injector.get(UserService);
        this.sqlite = injector.get(SqliteService);
        this.network = injector.get(NetworkService);
        this.utility = injector.get(UtilityService);
        this.location = injector.get(Location);
        this.events = injector.get(EventsService);
        this.nav = injector.get(NavService);
        this.propertyService = injector.get(PropertyService);

        // this.formBuilder = injector.get(FormBuilder);
        this.popover = injector.get(PopoversService);
        this.modals = injector.get(ModalService);
        this.router = injector.get(Router);
        this.activatedRoute = injector.get(ActivatedRoute);

        this.events.subscribe("getQueryParams", this.getQueryParams.bind(this))

       
    }

    getParams() {
        return this.activatedRoute.snapshot.params;
    }
  
    getQueryParams() {
        return this.activatedRoute.snapshot.queryParams;
    }

   
    
}