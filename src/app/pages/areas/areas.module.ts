import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';  
import { SharedModule } from '../../shared/shared.module';
import { AreasComponent } from './areas.component';
import { AreaComponent } from './area/area.component';


export const routes = [
  { path: '', component: AreasComponent, pathMatch: 'full' },
  { path: ':id', component: AreaComponent }
];

@NgModule({
  declarations: [
    AreasComponent, 
    AreaComponent
  ],
  exports: [
    AreasComponent, 
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AgmCoreModule,
    SharedModule
  ]
})
export class AreasModule { }
