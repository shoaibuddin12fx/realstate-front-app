import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnquireNowComponent } from './enquire-now.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes = [
  {
    path: '',
    component: EnquireNowComponent,
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [EnquireNowComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class EnquireNowModule { }
