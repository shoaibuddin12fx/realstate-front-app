import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent extends BasePage implements OnInit {
  wpPosts;
  constructor(private inj: Injector) { 
    super(inj);
  }

  ngOnInit() {
    this.getWordpressPosts();
  }
  getWordpressPosts(){
    this.network.getWordpressAboutUs().subscribe((response: any) => {
      
      this.wpPosts = response.find((x) => x.slug == 'about-us')
      console.log(this.wpPosts, 'Resp:');

    })
  }
}
