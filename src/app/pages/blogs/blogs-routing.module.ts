import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogsSlugComponent } from './blogs-slug/blogs-slug.component';
import { BlogsComponent } from './blogs.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: BlogsComponent,
  },
  { 
    path: 'list/:id',
    component: BlogsSlugComponent,
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogsRoutingModule { }
