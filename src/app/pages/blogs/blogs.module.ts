import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogsRoutingModule } from './blogs-routing.module';
import { BlogsComponent } from './blogs.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BlogsSlugComponent } from './blogs-slug/blogs-slug.component';


@NgModule({
  declarations: [BlogsComponent, BlogsSlugComponent, ],
  imports: [
    CommonModule,
    BlogsRoutingModule,
    SharedModule
  ]
})
export class BlogsModule { }
