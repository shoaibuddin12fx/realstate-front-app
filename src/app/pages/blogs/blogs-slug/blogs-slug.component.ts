import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'app-blogs-slug',
  templateUrl: './blogs-slug.component.html',
  styleUrls: ['./blogs-slug.component.scss']
})
export class BlogsSlugComponent implements OnInit {
  slug = '';
  blog = null;
  wpPosts = null;

  private unSubscribe$ = new Subject();


  constructor(private activatedRoute: ActivatedRoute, private network: NetworkService) { 
    this.getSlug();
  }

  getSlug(){
    this.activatedRoute.params.subscribe((params) => {
      console.log('params',{params}); //log the entire params object
      this.getSingleBlog(params.id)
      this.getWordpressPosts(params.id);

  });
  }

  getSingleBlog(id){
    this.network.getBlogs().pipe(takeUntil(this.unSubscribe$)).subscribe((response: any) => {
      this.blog = response.post.find((x) => x.ID == id);
      console.log('Response: Single Blogs', this.blog);
    })
  }

  getWordpressPosts(id){
    this.network.getWordpressPosts(id).pipe(takeUntil(this.unSubscribe$)).subscribe((response: any) => {
      this.wpPosts = response
      console.log('getWordpressPosts: Single Blogs', this.wpPosts);
    })
  }

  ngOnInit(): void {
  }

  getDay(dateReceived: any){
    const months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
    const days = [
      'Sun',
      'Mon',
      'Tue',
      'Wed',
      'Thu',
      'Fri',
      'Sat'
    ]
  const d = new Date(dateReceived)
  const year = d.getFullYear() // 2019
  const date = d.getDate() // 23
  const monthName = months[d.getMonth()]
  const dayName = days[d.getDay()] // Thu
  const formatted = `${dayName}, ${date} ${monthName} ${year}`

    
  return formatted;
    
  }

}
