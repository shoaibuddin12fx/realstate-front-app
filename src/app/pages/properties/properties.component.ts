import {
  AfterViewInit,
  Component,
  Inject,
  Injector,
  OnChanges,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  ViewChild,
} from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MediaChange, MediaObserver } from "@angular/flex-layout";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { Subject, Subscription } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  takeUntil,
} from "rxjs/operators";
import { Settings, AppSettings } from "../../app.settings";
import { AppService } from "../../app.service";
import { Property, Pagination } from "../../app.models";
import { isPlatformBrowser } from "@angular/common";
import { NetworkService } from "src/app/services/network.service";
import { PropertyService } from "src/app/services/data-services/property.service";
import { BasePage } from "../base-page/base-page";
import { query } from "@angular/animations";

@Component({
  selector: "app-properties",
  templateUrl: "./properties.component.html",
  styleUrls: ["./properties.component.scss"],
})
export class PropertiesComponent extends BasePage implements OnInit, OnDestroy {
  @ViewChild("sidenav") sidenav: any;
  public sidenavOpen: boolean = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public psConfig: PerfectScrollbarConfigInterface = {
    wheelPropagation: true,
  };

  queryParams: any;
  public properties: any = [];
  public viewType: string = "grid";
  public viewCol: number = 33.3;
  public count: number = 12;
  public sort: string;
  public searchFields: any;
  public removedSearchField: string;
  public pagination: Pagination = new Pagination(1, this.count, null, 2, 0, 0);
  public message: string = 'No data found';
  public watcher: Subscription;
  private unSubscribe$ = new Subject();

  public settings: Settings;
  constructor(
    public injector: Injector,
    public appSettings: AppSettings,
    public appService: AppService,
    public mediaObserver: MediaObserver
  ) {
    super(injector);
    this.settings = this.appSettings.settings;
    this.watcher = mediaObserver
      .asObservable()
      .pipe(
        filter((changes: MediaChange[]) => changes.length > 0),
        map((changes: MediaChange[]) => changes[0])
      )
      .subscribe((change: MediaChange) => {
        if (change.mqAlias == "xs") {
          this.sidenavOpen = false;
          this.viewCol = 100;
        } else if (change.mqAlias == "sm") {
          this.sidenavOpen = false;
          this.viewCol = 50;
        } else if (change.mqAlias == "md") {
          this.viewCol = 50;
          this.sidenavOpen = true;
        } else {
          this.viewCol = 33.3;
          this.sidenavOpen = true;
        }
      });
  }

  ngOnInit() {
    this.properties = [];
    this.activatedRoute.queryParams.subscribe((queryParams) => {
      console.log({ queryParams });
      this.queryParams = queryParams;
      this.properties = this.propertyService.getFilteredDataForRoutes(
        queryParams
      );
      
    });
  }

  

  
  // public resetPagination(){
  //   if(this.paginator){
  //     this.paginator.pageIndex = 0;
  //   }
  //   this.pagination = new Pagination(1, this.count, null, null, this.pagination.total, this.pagination.totalPages);
  // }

  public filterData(data) {
    return this.appService.filterData(
      data,
      this.searchFields,
      this.sort,
      this.pagination.page,
      this.pagination.perPage
    );
  }

  public searchClicked(event) {
    console.log({ event });

    // if (isPlatformBrowser(this.platformId)) {
    //   window.scrollTo(0,0);
    // }
  }

  public searchChanged(event) {
    console.log({ event });

    event.valueChanges.subscribe(() => {
      this.searchFields = event.value;

      console.log("search clicked: ", this.searchFields);
      setTimeout(() => {
        this.removedSearchField = null;
      });
      if (!this.settings.searchOnBtnClick) {
        this.properties.length = 0;
      }
    });
    event.valueChanges
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(() => {
        this.properties = this.propertyService.filterPropertyTypeLocally(
          this.searchFields
        );
      });
  }

  public removeSearchField(field) {
    //   this.message = null;
    //   this.removedSearchField = field;
  }

  public changeCount(count) {
    //   this.count = count;
    //   this.properties.length = 0;
    //   this.resetPagination();
    //   this.getProperties();
  }
  public changeSorting(sort) {
    //   this.sort = sort;
    //   this.properties.length = 0;
    //   this.getProperties();
  }
  public changeViewType(obj) {
    //   this.viewType = obj.viewType;
    //   this.viewCol = obj.viewCol;
  }

  public onPageChange(e) {
    //   this.pagination.page = e.pageIndex + 1;
    //   this.getProperties();
    //   if (isPlatformBrowser(this.platformId)) {
    //     window.scrollTo(0,0);
    //   }
  }

  ngOnDestroy() {
    this.unSubscribe$.unsubscribe();
    this.watcher.unsubscribe();
  }
}
