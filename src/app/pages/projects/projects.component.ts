import { Component, Inject, Injector, Input, OnChanges, OnInit, PLATFORM_ID, SimpleChange, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { Subscription } from 'rxjs'; 
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators'; 
import { Settings, AppSettings } from '../../app.settings';
import { AppService } from '../../app.service';
import { Property, Pagination } from '../../app.models'; 
import { isPlatformBrowser } from '@angular/common';
import { VariablesService } from 'src/app/services/variables.service';
import { BasePage } from '../base-page/base-page';
import { ProjectService } from 'src/app/services/data-services/project.service';


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent extends BasePage implements OnInit, OnChanges {
  @ViewChild('sidenav') sidenav: any;

  public sidenavOpen:boolean = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public psConfig: PerfectScrollbarConfigInterface = {
    wheelPropagation:true
  };
  public properties: Property[]; 
  public projects;
  public galleryImages;
  public viewType: string = 'grid';
  public viewCol: number = 33.3;
  public count: number = 12;
  public sort: string;
  public searchFields: any;
  public removedSearchField: string;
  public pagination:Pagination = new Pagination(1, this.count, null, 2, 0, 0); 
  public message:string;
  public watcher: Subscription;

  public queryParams: any;
  public settings: Settings;

  constructor(injector: Injector, public appSettings:AppSettings, 
              public mediaObserver: MediaObserver,
              public projectService: ProjectService,
              ) {
    super(injector); 
    this.settings = this.appSettings.settings;    
    this.watcher = mediaObserver.asObservable()
    .pipe(filter((changes: MediaChange[]) => changes.length > 0), map((changes: MediaChange[]) => changes[0]))
    .subscribe((change: MediaChange) => {
      if (change.mqAlias == 'xs') {
        this.sidenavOpen = false;
        this.viewCol = 100;
      }
      else if(change.mqAlias == 'sm'){
        this.sidenavOpen = false;
        this.viewCol = 50;
      }
      else if(change.mqAlias == 'md'){
        this.viewCol = 50;
        this.sidenavOpen = true;
      }
      else{
        this.viewCol = 33.3;
        this.sidenavOpen = true;
      }
    });
    console.log('This activated route: ', this.getQueryParams().slug);
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.queryParams = queryParams;
      this.getProjects(queryParams.slug)
    });

  }

  
  ngOnChanges() {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.

    
  }

  ngOnInit() {
    
  }

 

  async getProjects(slug){  
    const projects: any = await this.projectService.getProjects();
    if(slug){
      this.projects = projects.filter((x) => x.projectType != 'off-plan-project' && x.projectType != '' && x.projectType == slug);
    }
    else{
      this.projects = projects.filter((x) => x.projectType != 'off-plan-project' && x.projectType != '');
    }
}

  public resetPagination(){ 
    if(this.paginator){
      this.paginator.pageIndex = 0;
    }
    this.pagination = new Pagination(1, this.count, null, null, this.pagination.total, this.pagination.totalPages);
  }

  public filterData(data){
    // return this.appService.filterData(data, this.searchFields, this.sort, this.pagination.page, this.pagination.perPage);
  }

  public searchClicked(){ 
    // this.properties.length = 0;
    // this.getProperties(); 
    // if (isPlatformBrowser(this.platformId)) {
    //   window.scrollTo(0,0);
    // }  
  }
  public searchChanged(event){
    console.log({event});
    
    event.valueChanges.subscribe(() => {   
      this.resetPagination(); 
      this.searchFields = event.value;
      setTimeout(() => {      
        this.removedSearchField = null;
      });
      if(!this.settings.searchOnBtnClick){     
        this.properties.length = 0;  
      }            
    }); 
    event.valueChanges.pipe(debounceTime(500), distinctUntilChanged()).subscribe(() => { 
      this.projects = this.projectService.filterProjects(this.searchFields)
    });       
  } 
  public removeSearchField(field){ 
    // this.message = null;   
    // this.removedSearchField = field; 
  } 


  public changeCount(count){
    // this.count = count;   
    // this.properties.length = 0;
    // this.resetPagination();
    // this.getProperties();
  }
  public changeSorting(sort){    
    // this.sort = sort; 
    // this.properties.length = 0;
    // this.getProperties();
  }
  public changeViewType(obj){ 
    // this.viewType = obj.viewType;
    // this.viewCol = obj.viewCol; 
  } 


  public onPageChange(e){ 
    // this.pagination.page = e.pageIndex + 1;
    // this.getProperties();
    // if (isPlatformBrowser(this.platformId)) {
    //   window.scrollTo(0,0);
    // } 
  }

  ngOnDestroy(){ 
    this.watcher.unsubscribe();
  }

}