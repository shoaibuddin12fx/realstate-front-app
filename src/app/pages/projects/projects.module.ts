import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';  
import { SharedModule } from '../../shared/shared.module';
import { ProjectsComponent } from './projects.component';
import { ProjectComponent } from './project/project.component';


export const routes = [
  { path: '', component: ProjectsComponent, pathMatch: 'full' },
  { path: ':id', component: ProjectComponent }
];

@NgModule({
  declarations: [
    ProjectsComponent, 
    ProjectComponent
  ],
  exports: [
    ProjectsComponent, 
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AgmCoreModule,
    SharedModule
  ]
})
export class ProjectsModule { }
