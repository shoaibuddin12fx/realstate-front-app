import { Component, Injector, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CompanyModel } from 'src/app/app.models';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { NetworkService } from 'src/app/services/network.service';
import { emailValidator } from '../../utils/app-validators';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent extends BasePage implements OnInit, OnDestroy {
  
  company = [];
  columns: any;
  columnTwo = [];
  columnThree = [];
  columnFour = [];
  columnFive = [];
  columnSix = [];
  columnSeven = [];
  

  //Unsubscribe!
  private unSubscribe$ = new Subject();
  
  constructor(public inj: Injector, public formBuilder: FormBuilder) { 
    super(inj)
  }

  ngOnInit() {
    this.getCompany(); 
    this.getFooter();

  }

  getCompany(){
    this.network.getCompany().pipe(takeUntil(this.unSubscribe$)).subscribe((response: any) => {
      this.company = response.data;
    })
  }

  getFooter(){
    this.network.getFooterLinks().pipe(takeUntil(this.unSubscribe$)).subscribe((response: any) => {
      this.columns = response;
      console.log('columns now: ', response);
      
    })
  }
  route(url){
    const urlEncode = encodeURI(url);
    console.log({urlEncode});

    this.nav.navigateToChildByUrl(urlEncode, this.activatedRoute.snapshot)

    
  }



  ngOnDestroy(){
    this.unSubscribe$.complete();
  }

}
