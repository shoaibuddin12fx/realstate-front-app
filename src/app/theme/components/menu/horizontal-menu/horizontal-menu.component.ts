import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MenuService } from '../menu.service';
import { Menu } from '../menu.model';
import { DataService } from 'src/app/services/data.service';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { inject } from '@angular/core/testing';
import { Injector } from '@angular/core';

@Component({
  selector: 'app-horizontal-menu',
  templateUrl: './horizontal-menu.component.html',
  styleUrls: ['./horizontal-menu.component.scss'],
  providers: [ MenuService ]
})
export class HorizontalMenuComponent extends BasePage implements OnInit, OnChanges {
  @Input('menuParentId') menuParentId;
  submenuToggle = false;
  public menuItems;
  constructor(public inject: Injector,public menuService:MenuService, public dataService: DataService,) {
    super(inject)
   }

  ngOnInit() {
    // this.menuItems = this.menuService.getHorizontalMenuItems();
    
  }


  ngOnChanges(){
    console.log(this.menuParentId );
      // this.dataService.getFilteredChilds(this.menuParentId)

  }

  route(url){
    const urlEncode = encodeURI(url);
    this.nav.navigateToChildByUrl(urlEncode, this.activatedRoute.snapshot)

    
  }

  renderSubmenu(parentId){
    this.dataService.getFilteredChilds(parentId)
  }




}
