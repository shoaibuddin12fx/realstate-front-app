import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CompanyModel } from 'src/app/app.models';
import { AppService } from 'src/app/app.service';
import { JsonService } from 'src/app/services/json.service';

@Component({
  selector: 'app-toolbar1',
  styleUrls: ['./toolbar1.component.scss'],
  templateUrl: './toolbar1.component.html'
})
export class Toolbar1Component implements OnInit, OnDestroy {
  @Output() onMenuIconClick: EventEmitter<any> = new EventEmitter<any>();
  @Input('componentName') componentName: string;

  headersData: CompanyModel = null;
  private unsubscribe$ = new Subject();



  constructor(public appService:AppService, public jsonService:JsonService) {
    this.getHeadersData()
  } 

  ngOnInit() { 
    
  }

  getHeadersData(){
    this.jsonService.companyData$.pipe(takeUntil(this.unsubscribe$)).subscribe((response) => {
      this.headersData = response
    }) 
  }

  public sidenavToggle(){
    this.onMenuIconClick.emit();
  }


  ngOnDestroy(){
    this.unsubscribe$.complete()
    this.unsubscribe$.unsubscribe();
  }

}