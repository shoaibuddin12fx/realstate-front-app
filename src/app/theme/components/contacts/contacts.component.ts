import { Component, OnInit, Input } from '@angular/core';
import { CompanyModel } from 'src/app/app.models';
import { JsonService } from 'src/app/services/json.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  @Input() dividers:boolean = true;
  @Input() iconSize:string = 'sm';
  @Input() iconColor:string = '';
  @Input() headerData: CompanyModel = null;
  constructor(public jsonService: JsonService) { }

  ngOnInit() {

    
  }
 


}
