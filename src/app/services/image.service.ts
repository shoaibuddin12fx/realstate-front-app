import { Injectable } from '@angular/core';
import { AlertsService } from './basic/alerts.service';
import { ModalService } from './basic/modal.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(public alerts: AlertsService) { }

  snapImage(type) {

    return new Promise(async (resolve) => {

      // not needed in web

    });
  }

  cropWithController(imageData, type) {
    function getCropOptions() {

      if (type == "licence") {

        const c = {
          dragMode: 'crop',
          aspectRatio: 16 / 9,
          autoCrop: true,
          movable: true,
          zoomable: true,
          scalable: true,
          autoCropArea: 0.8,
        };

        return c;

      } else {

        const c = {
          dragMode: 'crop',
          aspectRatio: 1,
          autoCrop: true,
          movable: true,
          zoomable: true,
          scalable: true,
          autoCropArea: 0.8,
        };

        return c;

      }

    }

    return new Promise(async (resolve, reject) => {

      

    })


  }

  convertImageUrltoBase64(url) {
    console.log("here-url-", url);
    
    return new Promise((resolve) => {
      if(!url){
        resolve(null);
      }else{
        if (!this.isValidUrl(url) || (/^http/.test(url))) {
          var index = url.lastIndexOf("/") + 1;
          var filename = url.substr(index);
          resolve(filename);
        } else {
          this.convertToDataURLviaCanvas(url).then(base64 => {
            resolve(this.getRB64fromB64(base64));
          })
        }
      }
      

    });
  }

  isValidUrl = (string) => {
    try {
      new URL(string);
      return true;
    } catch (_) {
      return false;
    }
  }

  private convertToDataURLviaCanvas(url, outputFormat = "image/jpeg") {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = () => {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'),
          dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        resolve(dataURL);
        canvas = null;
      };
      img.src = url;
    });
  }

  public getRB64fromB64(str) {
    return str.substring(str.indexOf(",") + 1);
  }

}
