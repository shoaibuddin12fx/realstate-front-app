import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class DataService implements OnDestroy {
  private unsubscribe$ = new Subject();
  menus = []
  subMenus = []
  localMenus = []

  constructor(public network: NetworkService) { 
    this.getMenusPartTwo();
  }

  getAreas() {
    return new Promise( resolve => {

        this.network.getMenu().subscribe(
            (res: any) => {
                console.log(res.menu);
                resolve(res.menu);
            },
            (error) => { 
                console.log(resolve(error))
            },
        );
    })
    
}
 

  getMenusPartTwo(){
    this.network.getMenu().pipe(takeUntil(this.unsubscribe$)).subscribe((resp: any) => {
      this.menus = resp.menu;
      console.log('Hello Server menus', resp);
    })
  }

  getFilteredChilds(id: number){
      this.subMenus =  this.menus.filter(item => item.parentId == id); 
      console.log('this.dataService.menus Check', this.subMenus);
  }


  ngOnDestroy(){
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
