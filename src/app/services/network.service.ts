import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EventsService } from './events.service';
import { ApiService } from './api.service';
import { UtilityService } from './utility.service';



@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(
    public utility: UtilityService,
    public api: ApiService,
  ) {
    // console.log('Hello NetworkProvider Provider');
  }

  // post requests -- start
  isUserExistWithPhoneNumber(data) {
    return this.httpPostResponse('is_user_exist_with_phonenumber', data, null, false)
  }

  mainmanue(data) {
    return this.httpPostResponse('login', data, null, true)
  }

  register(data) {
    return this.httpPostResponse('signup', data, null, true)
  }

    // post requests -- ends

  // get requests -- start

  
  getMenu() {
    return this.api.get('menus')
  }

  getSliders() {
    return this.api.get('sliders')
  }

  getProjects() {
    return this.api.get('projects')
  }

  getSingleProject(id) {
    return this.api.get('projects/' + id)
  }

  getOffPlanProjects() {
    return this.api.get('off-plan-projects')
  }

  getBlogs() {
    return this.api.get('wp_posts')
  }

  getAreas() {
    return this.api.get('areas')
  }

  getSingleArea(id) {
    return this.api.get('areas/' + id)
  }

  getProperties() {
    return this.api.get('properties')
  }

  getWhyUs() {
    return this.api.get('why-us')
  }

  getCompany() {
    return this.api.get('company')
  }

  getFooterLinks() {
    return this.api.get('footer')
  }

  getWordpressPosts(id) {
    return this.api.getWordpress('posts/' + id)
  }
  getWordpressAboutUs() {
    return this.api.getWordpress('posts')
  }

  getSingleProperty(id) {
    return this.api.get('properties/' + id)
  }

  getPropertyGalleryImages() {
    return this.api.get('propertyGallery')
  }

  getProjectGalleryImages() {
    return this.api.get('projectGallery')
  }

  

  // get requests -- end




  httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
  }

  httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    
    return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
  }

  // default 'Content-Type': 'application/json',
  httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json'): Promise<any> {

    return new Promise( ( resolve, reject ) => {

      if (showloader == true) {
        this.utility.showLoader();
      }
      const _id = (id) ? '/' + id : '';
      const url = key + _id;

      const seq = (type == 'get') ? this.api.get(url, {}) : this.api.post(url, data);
      console.log({seq});
      
      seq.subscribe((res: any) => {
        if (showloader == true) {
          this.utility.hideLoader();
        }
        resolve(res)
        
        // this.utility.presentSuccessToast(res['message']);

      }, err => {

        let error = err['error'];
        if (showloader == true) {
          this.utility.hideLoader();
        }

        if (showError) {
          this.utility.presentFailureToast(error['message']);
        }

        console.log(err);

        reject(null);

      });

    });

  }

  showFailure(err) {
    // console.error('ERROR', err);
    var _error = (err) ? err['message'] : "check logs";
    this.utility.presentFailureToast(_error);
  }

  getActivityLogs() {
    return this.httpGetResponse('get_activity_logs', null, true)
  }

}
