import { StringsService } from './strings.service';
import { Injectable } from '@angular/core';
const Swal = require('sweetalert2')

@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor(
    public strings: StringsService
     ) { }

  showAlert(msg): Promise<any> {
    return new Promise(async resolve => {
      
      // will come back to this 
      resolve(true);
    })

  }

  async presentSuccessToast(msg) {

    
  }

  async presentFailureToast(msg) {
    
  }

  async presentToast(msg) {
    
  }

  presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = '') : Promise<boolean>{
    return new Promise( async resolve => {
      
    })
  }

  presentRadioSelections(title, message, inputs, okText = 'OK', cancelText = 'Cancel'): Promise<any>{
    return new Promise( async resolve => {
      
    })

  }

}
