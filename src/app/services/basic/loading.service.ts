import { Injectable } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  loading;
  constructor(public loadingController: NgxSpinnerService) {}

  
  async showLoader(message = 'Please wait...') {
    this.loading = await this.loadingController.show();
    
  }

  async hideLoader(){
    this.loading.hide();
  }
  
}
