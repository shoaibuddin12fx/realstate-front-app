import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }

  present(component, data = {}, cssClass = ''): Promise<any>{

    return new Promise( async resolve => {

      // will get back to this in web

      // const modal = await this.modal.create({
      //   component,
      //   cssClass,
      //   componentProps: data
      // });
      // modal.onDidDismiss().then( data => {
      //   resolve(data);
      // });
      // await modal.present();
    });

  }

  dismiss(data: any = {}): Promise<any>{
    return new Promise( resolve => {
      data['dismiss'] = true;
      resolve(true);
      // this.modal.dismiss(data).then( v => resolve() );
    });
  }

}
