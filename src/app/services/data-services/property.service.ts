import { Injectable, OnDestroy } from "@angular/core";
import { BehaviorSubject, Observable, Subject } from "rxjs";

import { NetworkService } from "../network.service";

@Injectable({
  providedIn: "root",
})
export class PropertyService {
  /* Arrays */
  public properties = [];
  public property;

  constructor(private network: NetworkService) {
    this.getProperties();
  }

  /**** Properties ****/
  getProperties() {
    return new Promise((resolve) => {
      this.network.getProperties().subscribe(
        (res: any) => {
          console.log("GetProperties: ", res);
          this.properties = res.data;
          resolve(res.data);
        },
        (error) => {
          console.log(resolve(error));
        }
      );
    });
  }

  getSingleProperty(id) {
    return new Promise((resolve) => {
      this.network.getSingleProperty(id).subscribe(
        (res: any) => {
          resolve(res.property);
        },
        (error) => {
          resolve(false);
        }
      );
    });
  }

  getFilteredDataForRoutes(param?) {
    console.log("properties: ", this.properties);
    let prop = [];
    if (param.from == "home") {
      console.log("In service", { param });
      prop = this.properties.filter((x) => {
        if (x.filters != null && x.price != null) {
          if (!param) {
            return true;
          }
          if (!x.filters) {
            return false;
          }
          if (!x.price) {
            return false;
          }
          let filters = param.filters;
          let priceFrom = param.priceFrom;
          let priceTo = param.priceTo;
          // now add logic here
          let arr = Object.keys(JSON.parse(x.filters));
          console.log({ arr });

          console.log({ filters });
          if ((priceFrom && priceTo) != null) {
            if (x.price >= priceFrom && x.price <= priceTo) {
              return true;
            }
          } else if (filters.length > 0) {
            if (arr.some((r) => filters.includes(r))) {
              return true;
            }
          } else {
            if (
              arr.some((r) => filters.includes(r)) &&
              x.price >= priceFrom &&
              x.price <= priceTo
            ) {
              return true;
            }
          }

          return false;
        }
      });

      return prop;
    } else {
      console.log("In service route", { param });
      let prop = this.properties.filter((x) => {
        if (x.filters != null) {
          if (!param) {
            return true;
          }
          if (!x.filters) {
            return false;
          }
          let type = param.type;
          // now add logic here
          let arr = Object.keys(JSON.parse(x.filters));
          let arr2 = Object.keys(param);

          if (type) {
            let f = arr.find((x) => x == type);
            if (f) {
              if (arr.some((r) => arr2.includes(r))) {
                return true;
              }
            }
            return false;
          }
          return false;
        }
      });
      return prop;
    }
  }

  filterPropertyTypeLocally(param?) {
    console.log("In service", { param });
    let prop;
    if (param) {
      prop = this.properties
        .filter((x) => {
          if (x.filters) {
            if (!param) {
              return true;
            }
            if (!x.filters) {
              return false;
            }

            // now add logic here
            let arr = Object.keys(JSON.parse(x.filters));
            let filters = param.propertyType != null ? param.propertyType : [];

            console.log("In service filters", x.filters);

            if (filters.some((r) => arr.includes(r.name))) {
              return true;
            }
            return true;
          }
        })
        .filter((x) => {
          if (param.price.to && param.price.from) {
            console.log(param.price);
            let priceFrom = param.price.from;
            let priceTo = param.price.to;
            return x.price >= priceFrom && x.price <= priceTo;
          }
          return true;
        })
        .filter((x) => {
          if (param.city?.name) {
            return x.city == param.city.name;
          }
          return true;
        })
        .filter((x) => {
          if (param.bedrooms.to && param.bedrooms.from) {
            return (
              x.bedrooms >= param.bedrooms.from &&
              x.bedrooms <= param.bedrooms.to
            );
          }
          return true;
        })
        .filter((x) => {
          if (param.bathrooms.to && param.bathrooms.from) {
            return (
              x.washrooms >= param.bathrooms.from &&
              x.washrooms <= param.bathrooms.to
            );
          }
          return true;
        })
        .filter((x) => {
          if (param.garages.to && param.garages.from) {
            return x.park >= param.garages.from && x.park <= param.garages.to;
          }
          return true;
        });

      return prop;
    }
  }
}
