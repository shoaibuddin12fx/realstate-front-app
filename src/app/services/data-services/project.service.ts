import { Injectable, OnDestroy } from "@angular/core";
import { BehaviorSubject, Observable, Subject } from "rxjs";

import { NetworkService } from "../network.service";

@Injectable({
  providedIn: "root",
})
export class ProjectService {
  /* Arrays */
  public projects = [];
  public project;

  constructor(private network: NetworkService) {}

  /**** Projects ****/
  getProjects() {
    return new Promise((resolve) => {
      this.network.getProjects().subscribe(
        (res: any) => {
          console.log("In service projects", res.data);
          console.log(res.data);
          this.projects = res.data;
          resolve(res.data);
        },
        (error) => {
          console.log(resolve(error));
        }
      );
    });
  }

  getSingleProject(id) {
    return new Promise((resolve) => {
      this.network.getSingleProject(id).subscribe(
        (res: any) => {
          resolve(res.data);
        },
        (error) => {
          resolve(false);
        }
      );
    });
  }

  getOffPlanProjects() {
    return new Promise((resolve) => {
      this.network.getOffPlanProjects().subscribe(
        (res: any) => {
          console.log("In service projects offPlan", res.data);

          console.log("off-plan-projects: ", res.data);
          resolve(res.data);
        },
        (error) => {
          console.log(resolve(error));
        }
      );
    });
  }

  filterProjects(param) {
    console.log("In service", { param });
    console.log("In service projects" , this.projects);
    let prop = this.projects.filter((x) => {
        if (!param.propertyType.slug) {
          return true;
        }
        
        let filters = param.propertyType.slug;
        if(filters == x.projectType){
            return true
        }

        return false;
    });

    return prop;
  }
}
