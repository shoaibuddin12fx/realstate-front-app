import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { NetworkService } from '../network.service';

@Injectable({
  providedIn: 'root'
})
export class AreaService  {

    /* Arrays */
    public areas = [];
    public area;


    constructor(private network: NetworkService) {
        
    }

    /**** Areas ****/
    getAreas() {
        return new Promise( resolve => {

            this.network.getAreas().subscribe(
                (res: any) => {
                    console.log('In service areas',res.data);
                    
                    console.log(res.data);
                    this.areas = res.data;
                    resolve(res.data);
                },
                (error) => { 
                    console.log(resolve(error))
                },
            );
        })
        
    }

    getSingleArea(id) {
        return new Promise( resolve => {

            this.network.getSingleArea(id).subscribe(
                (res: any) => {
                    resolve(res.area);
                },
                (error) => { 
                    
                    resolve(false)
                },
            );
        })
        
    }

    

}
