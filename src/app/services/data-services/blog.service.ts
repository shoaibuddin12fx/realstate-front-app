import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { NetworkService } from '../network.service';

@Injectable({
  providedIn: 'root'
})
export class BlogService  {

    constructor(private network: NetworkService) {}

    /**** Properties ****/
    getProperties() {
        return new Promise( resolve => {

            this.network.getBlogs().subscribe(
                (res: any) => {
                    console.log('In service property',res.data);
                    
                    console.log(res.data);
                    resolve(res.data);
                },
                (error) => { 
                    console.log(resolve(error))
                },
            );
        })
        
    }

    getSingleProperty(id) {
        return new Promise( resolve => {

            this.network.getSingleProperty(id).subscribe(
                (res: any) => {
                    resolve(res.property);
                },
                (error) => { 
                    
                    resolve(false)
                },
            );
        })
        
    }

    

}
