import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../config/main.config';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url = Config.api;
  wordpress = Config.wordpress
  constructor(public http: HttpClient) {
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    return this.http.get(this.url + endpoint, reqOpts );
  }

  getWordpress(endpoint: string, params?: any, reqOpts?: any) {
    return this.http.get(this.wordpress + endpoint, reqOpts );
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http.post(this.url + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.url + endpoint, body, reqOpts);
  }

 

  
}
