import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { EventsService } from './events.service';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  constructor(public location: Location,public router: Router, public activatedRoute: ActivatedRoute, public events: EventsService) { }

  setRoot(page, param = {}){
    this.navigateTo(page, param);
  }

  push(page, param = {}){
    alert()
    let extras: NavigationExtras = {
      queryParams: param
    }
    this.navigateTo(page, extras);
  }

  pop(){
    return new Promise<void>( resolve => {
      this.location.back();
      resolve();
    });
  }

  navigateTo(link, data?: NavigationExtras){
    console.log({link});
    this.router.navigate([link], data);
  }

  navigateByUrl(link, data?: NavigationExtras){
    console.log({link});
    this.router.navigateByUrl(link, data);
  }

  navigateToChild(link, data?: NavigationExtras){
      data.relativeTo = this.activatedRoute;
      this.router.navigate([link], data);
  }

  navigateToChildByUrl(link, data?: NavigationExtras){
    data.relativeTo = this.activatedRoute;
    this.events.publish("onRouteChange", {link, data})
    this.router.navigateByUrl(link, data);
}

  getParams() {
      return this.activatedRoute.snapshot.params;
  }

  getQueryParams() {
      return this.activatedRoute.snapshot.queryParams;
  }

}
