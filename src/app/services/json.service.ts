import { HttpClient } from "@angular/common/http";
import { Injectable  } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { Builders, CompanyModel } from "../app.models";
import { environment } from 'src/environments/environment';
import { response } from "express";

@Injectable({
    providedIn: 'root'
  })

  

  export class JsonService {

    /* Arrays */
    companyDetails: CompanyModel = null;
    builders: Builders[] = [];

    /* Subject */
    private _companyData = new BehaviorSubject<CompanyModel>(this.companyDetails);
    private _builders = new BehaviorSubject<Builders[]>(this.builders);


    /* Observable */
    companyData$: Observable<CompanyModel> = this._companyData.asObservable();
    builders$: Observable<Builders[]> = this._builders.asObservable();


  public url =  environment.url + '/assets/data/'; 

  constructor(public http:HttpClient) {

    this.getCompany();
    this.getBuilders();

   }

   public getCompany(){
    this.http.get<CompanyModel>( this.url + 'variables.json').subscribe((response) => {
      
      this.companyDetails = response;
      this._companyData.next(this.companyDetails);
    });
  }

  public getBuilders(){
    this.http.get<Builders[]>(this.url + 'builders.json').subscribe((response: any) => {
      this.builders = response
      this._builders.next(this.builders.slice())

    })
  }

    
  }