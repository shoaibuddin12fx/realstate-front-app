import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-key-card-large',
  templateUrl: './key-card-large.component.html',
  styleUrls: ['./key-card-large.component.scss']
})
export class KeyCardLargeComponent implements OnInit {
  @Input() plan;

  constructor() { }

  ngOnInit(): void {
  }

}
