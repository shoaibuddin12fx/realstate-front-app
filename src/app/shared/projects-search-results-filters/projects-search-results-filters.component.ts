import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-projects-search-results-filters',
  templateUrl: './projects-search-results-filters.component.html',
  styleUrls: ['./projects-search-results-filters.component.scss']
})
export class ProjectsSearchResultsFiltersComponent implements OnInit {
  @Input() searchFields: any;
  @Output() onRemoveSearchField: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() { }

  public remove(field){
    this.onRemoveSearchField.emit(field);
  }

}