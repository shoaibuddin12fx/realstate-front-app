import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-quote-box',
  templateUrl: './quote-box.component.html',
  styleUrls: ['./quote-box.component.scss']
})
export class QuoteBoxComponent implements OnInit {

  @Input() title = 'Real Estate Projects in Dubai';
  @Input() subtitle = 'When it comes to finding out about new projects, which are almost nearing completion or ready to occupy new developments and off-plan projects, which have been just launched, or those under-construction in Dubai, our ‘Home Finder section’ is just what you need. Home to Dubai’s comprehensive list of real estate projects every project listing provides in-depth details starting from artistic, live images and video gallery, fact sheet, location map, frequently asked questions (FAQs), and more on sub communities within the projects, their features and nearby places. Our aim to provide as much information as possible to help you understand thoroughly as you progress with your home buying and investments. Use our search tool to select your preferences in order to view the best properties available. Or give us a call for immediate service at 800-326- 800.';
  @Input() image = 'backup_table';
  @Input() reference = false;
  @Input() reference_bold = '';
  @Input() reference_line1 = '';
  @Input() reference_line2 = '';


  constructor() { }

  ngOnInit(): void {
  }

}
