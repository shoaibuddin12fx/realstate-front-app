import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AreaService } from 'src/app/services/data-services/areas.service';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'app-browse-properties',
  templateUrl: './browse-properties.component.html',
  styleUrls: ['./browse-properties.component.scss']
})
export class BrowsePropertiesComponent implements OnInit, OnDestroy {

  areas;


  private unSubscribe$ = new Subject();


  constructor(private areaService: AreaService) { }

  ngOnInit(): void {
    this.getBrowsingProperties();
  }

  async getBrowsingProperties(){
      this.areas = await this.areaService.getAreas();
      
  }

  ngOnDestroy(){
    this.unSubscribe$.complete();
  }

}
